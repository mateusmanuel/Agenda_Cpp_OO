#include <iostream>

#include "amigo.hpp"
#include "contato.hpp"

using namespace std;

int main () {
	
	amigo algumAmigo;
	contato algumContato;
		
	algumAmigo.setNome("Fulano");
	algumAmigo.setEmail("fulano@gmail.com");
	algumAmigo.setTelefone("5+55 (61) 9999 9999");	
	algumAmigo.setIdade(18);
	algumAmigo.setFacebook("Fulano Brother");
		
	cout << "Nome: " << algumAmigo.getNome() << endl;
	cout << "Email: " << algumAmigo.getEmail() << endl;
	cout << "Telefone: " << algumAmigo.getTelefone() << endl;
	cout << "Idade: " << algumAmigo.getIdade() << endl;
	cout << "Facebook: " << algumAmigo.getFacebook() << endl << endl;

	algumContato.setNome("Beltrano");
	algumContato.setEmail("beltrano@gamil.com");
	algumContato.setTelefone("+55 (61) 8888 8888");	
	algumContato.setReferencia("Trabalho");
		
	cout << "Nome: " << algumContato.getNome() << endl;
	cout << "Email: " << algumContato.getEmail() << endl;
	cout << "Telefone: " << algumContato.getTelefone() << endl;
	cout << "Referencia: " << algumContato.getReferencia() << endl;

	return 0;
}

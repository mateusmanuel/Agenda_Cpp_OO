#include "pessoa.hpp"
#include <string>

using namespace std;

Pessoa::Pessoa(){
	nome = "NoName";
	email = "noname@noname.com";
	telefone = "+55 (55) 555-5555";
}

Pessoa::Pessoa(string nome, string email, string telefone) {
    this->nome = nome;
	this->email = email;
	this->telefone = telefone;
}

string  Pessoa::getNome () {
        return nome;
}

string  Pessoa::getEmail () {
        return email;
}

string  Pessoa::getTelefone () {
        return telefone;
}

void Pessoa::setNome (string nome) {
        this->nome = nome;
}

void Pessoa::setEmail (string idade) {
        this->email = email;
}

void Pessoa::setTelefone (string telefone) {
        this->telefone = telefone;
}


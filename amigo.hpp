#ifndef AMIGO_H
#define AMIGO_H

#include <string>
#include "pessoa.hpp"

using namespace std;

class amigo : public Pessoa {
	private:
		int idade;
		string facebook;
	
	public:
		amigo ();
		amigo (string nome, string email, string telefone, int idade, string facebook);
		
		int getIdade ();
        string getFacebook ();
	
		void setIdade (int idade);
		void setFacebook (string facebook);
};

#endif

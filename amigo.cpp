#include "amigo.hpp"
#include <string>

using namespace std;

amigo::amigo(){
	setNome ("NoName");
	setEmail ("noname@noname.com");
	setTelefone ("+55 (55) 555-5555");
	idade = 0;
	facebook = "NoName";
}

amigo::amigo(string nome, string email, string telefone, int idade, string facebook) {
    setNome (nome);
	setEmail (email);
	setTelefone (telefone);
	this->idade = idade;
	this->facebook = facebook;
}

int  amigo::getIdade () {
        return idade;
}

string  amigo::getFacebook () {
        return facebook;
}

void amigo::setIdade (int idade) {
        this->idade = idade;
}

void amigo::setFacebook (string facebook) {
        this->facebook = facebook;
}

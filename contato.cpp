#include "contato.hpp"
#include <string>

using namespace std;

contato::contato(){
	setNome ("NoName");
	setEmail ("noname@noname.com");
	setTelefone ("+55 (55) 555-5555");
	referencia = "NoName from Work";
}

contato::contato(string nome, string email, string telefone, string referencia) {
    setNome (nome);
	setEmail (email);
	setTelefone (telefone);
	this->referencia = referencia;
}

string  contato::getReferencia () {
        return referencia;
}

void contato::setReferencia (string referencia) {
        this->referencia = referencia;
}

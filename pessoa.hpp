#ifndef PESSOA_H
#define PESSOA_H

#include <string>

using namespace std;

class Pessoa {
	private:
        string nome;
        string email;
		string telefone;
	
	public:
        Pessoa ();
        Pessoa (string nome, string email, string telefone);
        
        string getNome ();
        string getEmail ();
        string getTelefone ();
        
		void setNome (string nome);
		void setEmail (string email);
		void setTelefone (string telefone);
		
};

#endif

#ifndef CONTATO_H	
#define CONTATO_H

#include <string>
#include "pessoa.hpp"

using namespace std;

class contato : public Pessoa {
	private:
		string referencia;
	
	public:
		contato ();
		contato (string nome, string email, string telefone, string referencia);
		
        string getReferencia ();
		void setReferencia (string referencia);
};

#endif
